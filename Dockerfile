#FROM debian:buster-20210311
FROM renefonseca/wasmtime:latest


# clang compiler
RUN apt -y update
RUN apt install -y wget curl python3 python3-pip
WORKDIR /mysdk
RUN wget https://github.com/WebAssembly/wasi-sdk/releases/download/wasi-sdk-12/wasi-sdk-12.0-linux.tar.gz
RUN tar xvf wasi-sdk-12.0-linux.tar.gz
ENV CC="/mysdk/wasi-sdk-12.0/bin/clang --sysroot=/mysdk/wasi-sdk-12.0/share/wasi-sysroot"

# wasmer
RUN curl https://raw.githubusercontent.com/wasienv/wasienv/master/install.sh | sh
ENV WASIENV_DIR="/root/.wasienv"
ENV PATH="$WASIENV_DIR/bin:$PATH"
ENV WASMER_DIR="/root/.wasmer"
ENV WASMER_CACHE_DIR="$WASMER_DIR/cache"
ENV PATH="$WASMER_DIR/bin:$PATH:$WASMER_DIR/globals/wapm_packages/.bin"

WORKDIR /src
ENTRYPOINT ["/bin/bash"]
