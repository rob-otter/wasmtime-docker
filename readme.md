# wasmtime-docker

Containers where compilation of c code to wasm bytecode works with clang and wasicc and execution in wasmtime and with wasmer works.

## run compiler container:
- build compiler: `sudo docker build . -t wasm_container`
- run compiler container: `sudo docker run -it -v $(pwd):/src wasm_container`

## compile with clang
- compile with clang inside container: `$CC test.c -o test.wasm`
- (if you want to export the "add" function: `$CC test.c -o test.wasm -Wl,--export=add`)

## compile with wasicc
- compile with wasicc inside container `wasicc test.c -o test.wasm`

## run wasmtime
- execute wasmtime inside with built file from before: `wasmtime test.wasm`
- receive return value from wasm: `echo $?`
- (if you want to call the exported add function: `wasmtime test.wasm --invoke add 1 6`)
## run wasmer:
- execute wasmer inside container with built file: `wasmer test.wasm`

## references used:
- https://github.com/WebAssembly/wasi-sdk
- https://github.com/bytecodealliance/wasmtime/blob/main/docs/WASI-tutorial.md -> TODO: filesystem tutorial