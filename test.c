#include <stdio.h>

int main() {
    printf("hi xxx\n");
    return 42;
}

// compile with: $CC test.c -o test.wasm -Wl,--export=add
// to export the function
// call with: wasmtime test.wasm --invoke add 1 6
int add(int a, int b) {
    printf("%d\n", (a + b));
    return a + b;
}